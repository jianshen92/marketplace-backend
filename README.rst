Marketplace Backend
===================

A Backend for Frontend service for Pantasia marketplace.

.. contents::

Development
-----------

Setup
^^^^^

For setting up local development for the very first, run the folliwng one-off command
::

  $make dev-bootstrap


Running the service
^^^^^^^^^^^^^^^^^^^

Use the following command to run marketplace_backend service and all related services
::

  $make runserver


The development server is available at ``http://localhost:8000/``


Managing dependencies
^^^^^^^^^^^^^^^^^^^^^

All project dependencies are managed using `Poetry <https://python-poetry.org/>`__.
The project direct dependencies are listed in ``pyproject.toml``.

You might need to run
::

  $poetry install

in the local machine, mainly to have ``mypy`` (one of the pre-commit hooks) and related dependencies installed for ``pre-commit`` to run properly.


Coding Style
^^^^^^^^^^^^

This repository uses `pre-commit <https://pre-commit.com/#install>`__ tool to check and automatically fix any formatting isue before creating a git commit.

Run the following command to install pre-commit into your git hooks and have it run on every commmit:

::

  $pre-commit install

For more information on how it works, see the ``.pre-commit-config.yaml`` configuration file.
