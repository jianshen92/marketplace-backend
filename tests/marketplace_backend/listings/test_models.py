import pytest

from tests.helpers.factories import BidFactory, ListingFactory


@pytest.mark.django_db
def test_listing_base_price_ada():
    listing = ListingFactory(base_price_lovelace=1000)
    assert listing.base_price_ada == 0.001


@pytest.mark.django_db
def test_bid_price_ada():
    bid = BidFactory(price_lovelace=1000)
    assert bid.price_ada == 0.001
