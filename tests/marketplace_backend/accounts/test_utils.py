import pytest
from django.core import signing

from marketplace_backend.accounts.utils import (
    create_user,
    get_verification_code,
    send_verification_email,
    verify_code,
    verify_user,
)

from tests.helpers.factories import UserFactory


@pytest.mark.django_db
def test_create_user():
    email = "email@example.com"
    password = "password"
    user = create_user(email, password)
    assert user.email == email


@pytest.mark.django_db
def test_get_verification_code():
    email = "email@example.com"
    user = UserFactory(email=email)
    code = get_verification_code(user)
    assert signing.loads(code) == user.email


@pytest.mark.django_db
def test_verify_code_success():
    email = "email@example.com"
    user = UserFactory(email=email)
    code = signing.dumps(user.email)
    assert verify_code(user, code)


@pytest.mark.django_db
def test_verify_code_fail():
    email = "email@example.com"
    user = UserFactory(email=email)
    code = "invalid"
    assert verify_code(user, code) is False


@pytest.mark.django_db
def test_verify_user():
    user = UserFactory(is_verified=False)
    verify_user(user)
    user.refresh_from_db()
    assert user.is_verified


def test_send_verification_email():
    send_verification_email()
