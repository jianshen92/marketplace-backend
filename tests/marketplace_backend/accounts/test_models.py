import pytest

from marketplace_backend.accounts.models import User


@pytest.mark.django_db
def test_create_user():
    email = "email@example.com"
    password = "password"
    user = User.objects.create_user(email, password)  # type: ignore[has-type]
    assert user.email == email


@pytest.mark.django_db
def test_create_user_no_email():
    email = None
    password = "password"

    with pytest.raises(ValueError):
        User.objects.create_user(email, password)  # type: ignore[has-type]


@pytest.mark.django_db
def test_create_superuser():
    email = "email@example.com"
    password = "password"
    user = User.objects.create_superuser(email, password)  # type: ignore[has-type]
    assert user.email == email


@pytest.mark.django_db
def test_create_superuser_not_staff():
    email = None
    password = "password"

    with pytest.raises(ValueError):
        User.objects.create_superuser(  # type: ignore[has-type]
            email, password, is_staff=False
        )


@pytest.mark.django_db
def test_create_superuser_not_superuser():
    email = None
    password = "password"

    with pytest.raises(ValueError):
        User.objects.create_superuser(  # type: ignore[has-type]
            email, password, is_superuser=False
        )
