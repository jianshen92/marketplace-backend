import pytest
from strawberry_django_jwt.refresh_token.shortcuts import create_refresh_token

from marketplace_backend.core.jwt import create_access_token, create_jwt_payload

from tests.helpers.factories import UserFactory


@pytest.mark.django_db
def test_token_create(client):
    password = "password"

    user = UserFactory()
    user.set_password(password)
    user.save()

    mutation = f"""
        mutation {{
            tokenCreate(email: "{user.email}", password: "{password}") {{
                payload {{
                    exp
                    origIat
                    email
                }}
                token
                refreshExpiresIn
                refreshToken
            }}
        }}
    """

    response = client.post(
        "/graphql/", {"query": mutation}, content_type="application/json"
    )

    json_response = response.json()

    assert json_response["data"]["tokenCreate"]["payload"]["email"] == user.email
    assert json_response["data"]["tokenCreate"]["payload"]["exp"] is not None
    assert json_response["data"]["tokenCreate"]["payload"]["origIat"] is not None
    assert json_response["data"]["tokenCreate"]["token"] is not None
    assert json_response["data"]["tokenCreate"]["refreshExpiresIn"] is not None
    assert json_response["data"]["tokenCreate"]["refreshToken"] is not None


@pytest.mark.django_db
def test_token_verify(client):
    user = UserFactory()
    payload = create_jwt_payload(user)
    token = create_access_token(payload)

    mutation = f"""
        mutation {{
            tokenVerify(token: "{token}") {{
                payload {{
                    exp
                    origIat
                    email
                }}
            }}
        }}
    """

    response = client.post("/graphql/", {"query": mutation})

    json_response = response.json()

    assert response.status_code == 200
    assert json_response["data"]["tokenVerify"]["payload"]["email"] == user.email
    assert json_response["data"]["tokenVerify"]["payload"]["origIat"] == payload.origIat
    assert json_response["data"]["tokenVerify"]["payload"]["exp"] == payload.exp


@pytest.mark.django_db
def test_token_refresh(client):
    user = UserFactory()
    refresh_token = create_refresh_token(user)

    mutation = f"""
        mutation {{
            tokenRefresh(refreshToken: "{refresh_token}") {{
                payload {{
                    exp
                    origIat
                    email
                }}
                token
                refreshToken
                refreshExpiresIn
            }}
        }}
    """

    response = client.post("/graphql/", {"query": mutation})

    json_response = response.json()

    assert response.status_code == 200
    assert json_response["data"]["tokenRefresh"]["payload"]["email"] == user.email
    assert json_response["data"]["tokenRefresh"]["payload"]["origIat"] is not None
    assert json_response["data"]["tokenRefresh"]["payload"]["exp"] is not None
    assert json_response["data"]["tokenRefresh"]["token"] is not None
    assert json_response["data"]["tokenRefresh"]["refreshToken"] is not None
    assert json_response["data"]["tokenRefresh"]["refreshExpiresIn"] is not None
