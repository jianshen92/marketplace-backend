import strawberry
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from strawberry_django_jwt.views import StatusHandlingGraphQLView as GQLView

from marketplace_backend.graphql_api.permissions import IsAuthenticated


def test_has_permission(client, settings):
    def resolve() -> str:  # pragma: no cover
        return "bar"

    @strawberry.type
    class TestPermissionsQueries:

        foo = strawberry.field(resolver=resolve, permission_classes=[IsAuthenticated])

    schema = strawberry.Schema(query=TestPermissionsQueries)

    class MockURLConf:
        urlpatterns = [path("graphql/", csrf_exempt(GQLView.as_view(schema=schema)))]

    settings.ROOT_URLCONF = MockURLConf

    query = """
        query {
            foo
        }
    """

    response = client.post("/graphql/", {"query": query})

    json_data = response.json()

    assert response.status_code == 200
    assert json_data["data"] is None
    assert json_data["errors"][0]["message"] == "User is not authenticated"
