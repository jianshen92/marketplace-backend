from django.db import models
from django.utils.translation import gettext_lazy as _


class Project(models.Model):
    policy_id = models.TextField(_("policy id"), unique=True)
    name = models.TextField(
        _("project name"),
        blank=True,
    )
    is_featured = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)


class Asset(models.Model):
    project = models.ForeignKey(Project, related_name="asset", on_delete=models.CASCADE)
    asset_id = models.TextField(_("asset id"), unique=True)
    asset_name = models.TextField(
        _("asset name"),
    )
    asset_verbose_name = models.TextField(
        _("verbose asset name"),
    )
    ipfs_path = models.TextField(
        _("ipfs path"),
    )
    pinata_path = models.TextField(
        _("pinata path"),
    )
    metadata = models.JSONField(_("onchain metadata"))
