from django.contrib import admin

from marketplace_backend.wallets.models import Wallet


# https://github.com/typeddjango/django-stubs/issues/507
# the alternative is monkeypatch
class WalletAdmin(admin.ModelAdmin):  # type: ignore[type-arg]
    list_display = (
        "user",
        "wallet_address",
        "stake_address",
        "is_verified",
    )
    list_display_links = (
        "user",
        "wallet_address",
        "stake_address",
    )
    search_fields = (
        "wallet_address",
        "stake_address",
    )


admin.site.register(Wallet, WalletAdmin)
