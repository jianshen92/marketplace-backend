from typing import List, Optional, Tuple, Union

from django.db import connections

from marketplace_backend.accounts.models import User
from marketplace_backend.assets.models import Project
from marketplace_backend.assets.utils import (
    get_blockchain_asset_by_id,
    get_project_details,
)
from marketplace_backend.graphql_api.assets.types.assets import (
    BlockchainAsset,
    BlockchainAssets,
)
from marketplace_backend.listings.models import Listing
from marketplace_backend.utils.db import dictfetchall
from marketplace_backend.wallets.models import Wallet


def get_assets_from_cardano_db(
    stake_addresses: List[str],
    offset: int = 0,
    limit: int = 20,
    includes: Optional[List[str]] = None,
    excludes: Optional[List[str]] = None,
) -> BlockchainAssets:
    query = f"""
    WITH all_sa AS (
        SELECT
            id,
            view
        FROM stake_address
        WHERE view = ANY(%s)
    ),
    assets AS (
        SELECT DISTINCT ON (ma_tx_out.policy, ma_tx_out.name)
            ENCODE(
                CONCAT(ma_tx_mint.policy,
                "right"(CONCAT('\\', ma_tx_mint.name),
                '-3'::INTEGER))::BYTEA, 'hex'
            ) AS asset_id
            , ENCODE(ma_tx_out.policy, 'hex') AS policy_id
            , ENCODE(ma_tx_out.name, 'escape') AS asset_name
            , ma_tx_out.quantity AS quantity
            , tx_metadata.json AS asset_metadata
            , tx_metadata.key
        FROM utxo_view
        INNER JOIN all_sa
            ON utxo_view.stake_address_id = all_sa.id
        INNER JOIN ma_tx_out
            ON utxo_view.id = ma_tx_out.tx_out_id
        INNER JOIN ma_tx_mint
            ON ma_tx_out.policy = ma_tx_mint.policy
            AND ma_tx_out.name = ma_tx_mint.name
        INNER JOIN tx_metadata ON
            ma_tx_mint.tx_id = tx_metadata.tx_id
    )
    SELECT
        *
    FROM assets
    {"WHERE asset_id = ANY(%s)" if includes else ""}
    {"WHERE asset_id <> ALL(%s)" if excludes else ""}
    LIMIT %s
    OFFSET %s
    """

    values_list = [
        (
            "stake_addresses",
            stake_addresses,
        ),
        ("includes", includes),
        ("excludes", excludes),
        (
            "limit",
            limit,
        ),
        (
            "offset",
            offset,
        ),
    ]

    # This will exclude the field if the value is empty or None
    values = [value for _, value in values_list if value is not None]

    with connections["cardano_db"].cursor() as cursor:
        cursor.execute(query, values)
        rows = dictfetchall(cursor)
    return BlockchainAssets(assets=rows)


def get_listing_details_by_asset(
    raw_assets: BlockchainAssets, user: User
) -> dict[str, Listing]:
    """
    Get matching listing details from marketplace db given a list of asset
    """
    asset_ids = [asset.asset_id for asset in raw_assets.assets]
    listings = Listing.objects.filter(
        asset__asset_id__in=asset_ids, owner_wallet_address__user=user, is_active=True
    )

    # Constructing dict so that we can retrieve listing with O(1)
    # during the next steps
    listing_dict = {}
    for listing in listings:
        listing_dict[listing.asset.asset_id] = listing
    return listing_dict


def get_listing_assets_id_by_user(
    user: User,
) -> Tuple[dict[str, Listing], List[str]]:
    """
    Get matching listing details from marketplace db given a user
    """
    listings = Listing.objects.filter(owner_wallet_address__user=user, is_active=True)

    # Constructing dict so that we can retrieve listing with O(1)
    # during the next steps
    listing_dict = {}
    listing_asset_ids = []
    for listing in listings:
        asset_id = listing.asset.asset_id
        listing_dict[asset_id] = listing
        listing_asset_ids.append(asset_id)

    return listing_dict, listing_asset_ids


def get_listing_by_asset_id(asset_id: str, user: User) -> Optional[Listing]:
    """
    Get listing by asset id
    """
    listing: Optional[Listing] = None
    try:
        listing = Listing.objects.get(
            asset__asset_id=asset_id, owner_wallet_address__user=user, is_active=True
        )
    except Listing.DoesNotExist:
        pass
    return listing


def get_user_assets(
    user: User,
    offset: int,
    limit: int,
) -> Tuple[BlockchainAssets, dict[str, Listing]]:
    """
    Get user inventory without is_listed filter. First queries BC, then queries DB.
    """
    addresses = list(
        Wallet.objects.filter(user=user).values_list("stake_address", flat=True)
    )

    bc_asset = get_assets_from_cardano_db(
        stake_addresses=addresses,
        offset=offset,
        limit=limit,
    )
    listing_dict = get_listing_details_by_asset(raw_assets=bc_asset, user=user)
    return bc_asset, listing_dict


def get_user_assets_by_listed(
    user: User, offset: int, limit: int, is_listed: bool
) -> Tuple[BlockchainAssets, dict[str, Listing]]:
    """
    Get user inventory with is_listed filter. First queries DB, then queries BC.
    """
    addresses = list(
        Wallet.objects.filter(user=user).values_list("stake_address", flat=True)
    )
    listing_dict, listing_ids = get_listing_assets_id_by_user(user=user)

    if is_listed:
        bc_asset = get_assets_from_cardano_db(
            stake_addresses=addresses, offset=offset, limit=limit, includes=listing_ids
        )
    else:
        bc_asset = get_assets_from_cardano_db(
            stake_addresses=addresses, offset=offset, limit=limit, excludes=listing_ids
        )
    return bc_asset, listing_dict


def get_user_asset_by_id(
    user: User, asset_id: str
) -> Tuple[BlockchainAsset, Optional[Project], Optional[Listing]]:
    bc_asset = get_blockchain_asset_by_id(asset_id)
    project = get_project_details(policy_id=bc_asset.policy_id)
    listing = get_listing_by_asset_id(asset_id=asset_id, user=user)
    return bc_asset, project, listing


def get_user_first_wallet(user: User) -> Union[Wallet, None]:
    wallet = Wallet.objects.filter(user=user, is_verified=True).first()
    return wallet
