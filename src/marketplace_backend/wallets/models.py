from django.db import models
from django.utils.translation import gettext_lazy as _

from marketplace_backend import settings


class Wallet(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="wallets", on_delete=models.CASCADE
    )
    wallet_address = models.TextField(_("address of a wallet"))
    stake_address = models.TextField(
        _("stake address of a wallet"), unique=True, blank=True
    )
    is_verified = models.BooleanField(default=False)
