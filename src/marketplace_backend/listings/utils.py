from datetime import datetime
from typing import Optional

from marketplace_backend.assets.models import Asset
from marketplace_backend.graphql_api.listings.types.listing import ListingType
from marketplace_backend.listings.models import Listing
from marketplace_backend.wallets.models import Wallet


def create_new_listing(
    asset_id: str,
    asset: Asset,
    wallet: Wallet,
    listing_type: ListingType,
    base_price_lovelace: Optional[int],
    starts_at: Optional[datetime],
    ends_at: Optional[datetime],
) -> Listing:
    try:
        # django to_field typing issue
        current_listing = Listing.objects.get(asset__asset_id=asset_id, is_active=True)
        current_listing.is_active = False
        current_listing.save()
    except Listing.DoesNotExist:
        pass
    finally:
        listing = Listing.objects.create(
            asset=asset,
            listing_type=listing_type.value,
            owner_wallet_address=wallet,
            base_price_lovelace=base_price_lovelace,
            starts_at=starts_at,  # type: ignore[misc]
            ends_at=ends_at,
            is_active=True,
        )
    return listing
