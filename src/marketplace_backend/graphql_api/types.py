import json
from typing import Any, NewType

import strawberry
from strawberry.django.context import StrawberryDjangoContext
from strawberry.types import Info as StrawberryInfo

Info = StrawberryInfo[StrawberryDjangoContext, Any]

JSONScalar = strawberry.scalar(  # pragma: no cover
    NewType("JSONScalar", Any),  # type: ignore[arg-type]
    serialize=lambda v: v,
    parse_value=lambda v: json.loads(v),
    description=(
        "The GenericScalar scalar type represents a generic "
        "GraphQL scalar value that could be: List or Object."
    ),
)
