import strawberry
from strawberry_django_jwt.middleware import JSONWebTokenMiddleware

from marketplace_backend.graphql_api.accounts.schema import (
    AccountMutations,
    UserQueries,
)
from marketplace_backend.graphql_api.assets.schema import (
    ProjectMutations,
    ProjectQueries,
)
from marketplace_backend.graphql_api.assets.types.assets import Asset
from marketplace_backend.graphql_api.listings.schema import ListingMutations
from marketplace_backend.graphql_api.wallets.schema import (
    WalletMutations,
    WalletQueries,
)


@strawberry.type
class Query(
    UserQueries,
    ProjectQueries,
    WalletQueries,
):
    pass


@strawberry.type
class Mutation(
    AccountMutations,
    ProjectMutations,
    ListingMutations,
    WalletMutations,
):
    pass


schema = strawberry.Schema(
    query=Query, mutation=Mutation, types=(Asset,), extensions=(JSONWebTokenMiddleware,)
)
