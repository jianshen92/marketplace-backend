import strawberry
from strawberry.django import auto

from marketplace_backend.assets.models import Project as ProjectModel


@strawberry.django.type(ProjectModel)
class Project:
    name: auto
    policy_id: auto
    is_verified: auto
    is_featured: auto
