import strawberry
import strawberry.django

from marketplace_backend.graphql_api.assets.mutations import ProjectCreateInput
from marketplace_backend.graphql_api.assets.resolvers import resolve_project
from marketplace_backend.graphql_api.assets.types.projects import Project


@strawberry.type
class ProjectQueries:
    # couldn't user strawberry django filter here due to a bug in non list field
    project: Project = strawberry.django.field(resolver=resolve_project)
    projects: list[Project] = strawberry.django.field()


@strawberry.type
class ProjectMutations:
    project_create: Project = strawberry.django.mutations.create(ProjectCreateInput)
