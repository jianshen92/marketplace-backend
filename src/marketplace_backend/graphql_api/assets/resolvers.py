from marketplace_backend.assets.models import Project as ProjectModel
from marketplace_backend.graphql_api.assets.types.projects import Project


def resolve_project(policy_id: str) -> Project:
    project: ProjectModel = ProjectModel.objects.get(policy_id=policy_id)
    return Project(
        name=project.name,
        policy_id=project.policy_id,
        is_verified=project.is_verified,
        is_featured=project.is_featured,
    )
