from typing import Optional

import strawberry
from strawberry.django import auto

from marketplace_backend.assets.models import Project as ProjectModel
from marketplace_backend.graphql_api.listings.types.listing import Listing
from marketplace_backend.graphql_api.types import JSONScalar
from marketplace_backend.wallets.models import Wallet as WalletModel


@strawberry.django.type(ProjectModel)
class Project:
    name: auto
    policy_id: auto
    is_featured: auto
    is_verified: auto


@strawberry.type
class WalletAsset:
    asset_id: str
    asset_name: str
    quantity: int
    asset_verbose_name: str
    image: str
    listing: Optional[Listing]


@strawberry.type
class WalletAssetDetail:
    asset_id: str
    asset_name: str
    quantity: int
    asset_verbose_name: str
    image: str
    listing: Optional[Listing]
    project: Project
    metadata: JSONScalar


@strawberry.django.type(WalletModel)
class Wallet:
    user: strawberry.LazyType[  # type: ignore[name-defined]
        "User",
        "marketplace_backend.graphql_api.accounts.types.users",
    ]
    wallet_address: auto
    stake_address: auto
    is_verified: auto
