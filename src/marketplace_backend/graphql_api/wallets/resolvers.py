from typing import Optional

from marketplace_backend.accounts.models import User as UserModel
from marketplace_backend.graphql_api.assets.types.projects import Project
from marketplace_backend.graphql_api.types import Info
from marketplace_backend.graphql_api.wallets.types.wallets import (
    WalletAsset,
    WalletAssetDetail,
)
from marketplace_backend.wallets.utils import (
    get_user_asset_by_id,
    get_user_assets,
    get_user_assets_by_listed,
)


def resolve_assets(
    root: "WalletQueries",  # type: ignore[name-defined]
    info: Info,
    offset: int = 0,
    limit: int = 20,
    is_listed: Optional[bool] = None,
) -> list[WalletAsset]:
    user = info.context.request.user
    assert isinstance(user, UserModel)

    if is_listed is None:
        bc_asset, listings = get_user_assets(user, offset, limit)
    else:
        bc_asset, listings = get_user_assets_by_listed(user, offset, limit, is_listed)

    return [
        WalletAsset(
            asset_id=item.asset_id,
            asset_name=item.asset_name,
            quantity=item.quantity,
            asset_verbose_name=item.verbose_name,
            image=item.fast_image_src,
            listing=listings.get(item.asset_id),  # type: ignore[arg-type]
        )
        for item in bc_asset.assets
    ]


def resolve_asset(
    root: "WalletQueries", info: Info, asset_id: str  # type: ignore[name-defined]
) -> WalletAssetDetail:
    user = info.context.request.user
    assert isinstance(user, UserModel)

    bc_asset, project, listing = get_user_asset_by_id(user, asset_id)

    if project is None:
        project_type = Project(
            name="",
            policy_id=bc_asset.policy_id,
            is_featured=False,
            is_verified=False,
        )
    else:
        project_type = Project(
            name=project.name,
            policy_id=project.policy_id,
            is_verified=project.is_verified,
            is_featured=project.is_featured,
        )

    # This will get changed anyway
    return WalletAssetDetail(
        asset_id=bc_asset.asset_id,
        asset_name=bc_asset.asset_name,
        quantity=bc_asset.quantity,
        asset_verbose_name=bc_asset.verbose_name,
        image=bc_asset.fast_image_src,
        listing=listing,  # type: ignore[arg-type]
        project=project_type,
        metadata=bc_asset.metadata,
    )
