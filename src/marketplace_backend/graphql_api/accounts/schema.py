from __future__ import annotations

from typing import Optional

import strawberry
from strawberry_django_jwt import mutations as jwt_mutations

from marketplace_backend.graphql_api.accounts.mutations import (
    register_account,
    verify_account,
)
from marketplace_backend.graphql_api.accounts.resolvers import resolve_users
from marketplace_backend.graphql_api.accounts.types.users import User
from marketplace_backend.graphql_api.permissions import IsAuthenticated
from marketplace_backend.graphql_api.types import Info


def resolve_user(root: "UserQueries", info: Info) -> Optional[User]:
    user = info.context.request.user
    if not user.is_authenticated:
        return None
    # broken strawberry django typing
    return User(  # type: ignore[call-arg]
        display_name=user.display_name,
        email=user.email,
        is_verified=user.is_verified,
        verification_address=user.verification_address,
        wallets=user.wallets.all(),
    )


@strawberry.type
class UserQueries:
    users: list[User] = strawberry.field(resolver=resolve_users)
    user: User = strawberry.field(
        resolver=resolve_user, permission_classes=[IsAuthenticated]
    )


@strawberry.type
class AccountMutations:
    token_create = jwt_mutations.ObtainJSONWebToken.obtain
    token_verify = jwt_mutations.Verify.verify
    token_refresh = jwt_mutations.Refresh.refresh

    account_register = strawberry.mutation(resolver=register_account)
    account_verify = strawberry.mutation(resolver=verify_account)
