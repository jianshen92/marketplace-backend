from django.contrib.auth import get_user_model

from marketplace_backend.graphql_api.accounts.types.users import User


def resolve_users() -> list[User]:
    user_model = get_user_model()
    users = user_model.objects.all()  # type: ignore[has-type]
    return [
        User(  # type: ignore[call-arg]
            display_name=user.display_name,
            email=user.email,
            is_verified=user.is_verified,
            verification_address=user.verification_address,
            wallets=user.wallets.all(),
        )
        for user in users
    ]
