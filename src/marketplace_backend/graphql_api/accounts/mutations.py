import strawberry
from strawberry_django_jwt.object_types import TokenPayloadType

from marketplace_backend.accounts.models import User as UserModel
from marketplace_backend.accounts.utils import (
    create_user,
    send_verification_email,
    verify_code,
    verify_user,
)
from marketplace_backend.core.jwt import (
    calculate_refresh_expires_in,
    create_access_token,
    create_jwt_payload,
)
from marketplace_backend.graphql_api.accounts.types.accounts import (
    AccountRegisterRequest,
    AccountVerificationRequest,
)
from marketplace_backend.graphql_api.accounts.types.users import User
from marketplace_backend.graphql_api.types import Info


@strawberry.type
class AccountRegister:
    user: User
    token: str
    payload: TokenPayloadType
    refresh_expires_in: int


def register_account(
    info: Info, email: str, password: str, password_confirmation: str
) -> AccountRegister:
    account_register_request = AccountRegisterRequest(
        email=email, password=password, password_confirmation=password_confirmation
    )
    user = create_user(
        account_register_request.email,
        account_register_request.password_confirmation.get_secret_value(),
    )
    # Placeholder API
    send_verification_email()
    payload = create_jwt_payload(user)
    token = create_access_token(payload)
    refresh_expires_in = calculate_refresh_expires_in()

    # broken strawberry django typing
    strawberry_user = User(  # type: ignore[call-arg]
        display_name=user.display_name,
        email=user.email,
        is_verified=user.is_verified,
        verification_address=user.verification_address,
        wallets=[],
    )

    return AccountRegister(
        user=strawberry_user,
        token=token,
        payload=payload,
        refresh_expires_in=refresh_expires_in,
    )


@strawberry.type
class AccountVerify:
    user: User
    message: str


def verify_account(info: Info, email: str, code: str) -> AccountVerify:
    request = AccountVerificationRequest(email=email, code=code)

    user = UserModel.objects.get(email=request.email)  # type: ignore[has-type]
    if user.is_verified:
        return AccountVerify(user=user, message="User is already verified")

    if verify_code(user=user, code=request.code):
        verify_user(user)
        return AccountVerify(user=user, message="Verification successful")
    else:
        return AccountVerify(user=user, message="Verification code is invalid")
