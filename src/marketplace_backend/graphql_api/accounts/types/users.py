import strawberry
from django.contrib.auth import get_user_model
from strawberry.django import auto

from marketplace_backend.accounts.utils import get_verification_code
from marketplace_backend.graphql_api.wallets.types.wallets import Wallet


@strawberry.django.type(get_user_model())
class User:
    display_name: auto
    email: auto
    is_verified: auto
    verification_address: auto
    verification_code: str = strawberry.field(resolver=get_verification_code)
    wallets: list[Wallet] = strawberry.django.field()
