from typing import Any

from django.contrib.auth.password_validation import validate_password
from django.forms import ValidationError
from pydantic import BaseModel, EmailStr, SecretStr, validator


class AccountRegisterRequest(BaseModel):
    email: EmailStr
    password: SecretStr
    password_confirmation: SecretStr

    @validator("password_confirmation")
    def passwords_match(
        cls, value: SecretStr, values: dict[str, Any], **kwargs: Any
    ) -> SecretStr:
        if value != values["password"]:
            raise ValueError("passwords do not match")
        return value

    @validator("password_confirmation")
    def passwords_validate(
        cls, value: SecretStr, values: dict[str, Any], **kwargs: Any
    ) -> SecretStr:
        try:
            validate_password(value.get_secret_value())
        except ValidationError as err:
            raise ValueError(" ".join(err.messages).lower())
        return value


class AccountVerificationRequest(BaseModel):
    email: EmailStr
    code: str
