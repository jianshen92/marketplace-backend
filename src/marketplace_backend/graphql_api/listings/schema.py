import strawberry

from marketplace_backend.graphql_api.listings.mutations import create_listing
from marketplace_backend.graphql_api.permissions import IsAuthenticated


@strawberry.type
class ListingMutations:
    listing_create = strawberry.mutation(
        resolver=create_listing, permission_classes=[IsAuthenticated]
    )
