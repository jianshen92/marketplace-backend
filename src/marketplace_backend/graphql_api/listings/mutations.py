from datetime import datetime
from typing import Optional

from django.db import transaction
from graphql import GraphQLError

from marketplace_backend.accounts.models import User as UserModel
from marketplace_backend.assets.utils import get_or_create_asset
from marketplace_backend.graphql_api.listings.types.listing import Listing, ListingType
from marketplace_backend.graphql_api.types import Info
from marketplace_backend.listings.utils import create_new_listing
from marketplace_backend.wallets.utils import get_user_first_wallet


def create_listing(
    root: "ListingMutations",  # type: ignore[name-defined]
    info: Info,
    asset_id: str,
    listing_type: ListingType,
    base_price_lovelace: Optional[int] = None,
    starts_at: Optional[datetime] = None,
    ends_at: Optional[datetime] = None,
) -> Listing:
    user = info.context.request.user
    assert isinstance(user, UserModel)

    wallet = get_user_first_wallet(user)
    if wallet is None:
        raise GraphQLError("User wallet is not verified")

    with transaction.atomic():
        asset = get_or_create_asset(asset_id)
        listing = create_new_listing(
            asset_id=asset_id,
            asset=asset,
            wallet=wallet,
            listing_type=listing_type,
            base_price_lovelace=base_price_lovelace,
            starts_at=starts_at,
            ends_at=ends_at,
        )

    return Listing(  # type: ignore[call-arg]
        listing_type=listing.listing_type,
        created_at=listing.created_at,
        base_price_lovelace=listing.base_price_lovelace,
        base_price_ada=listing.base_price_ada,
        starts_at=listing.starts_at,
        ends_at=listing.ends_at,
        is_active=listing.is_active,
    )
