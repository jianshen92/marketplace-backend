import enum
from typing import Optional

import strawberry.django
from strawberry.django import auto

from marketplace_backend.listings.models import Listing as ListingModel


@strawberry.enum
class ListingType(enum.Enum):
    FIXED_PRICE = "FIXED_PRICE"
    OPEN_TO_BID = "OPEN_TO_BID"
    AUCTION = "AUCTION"


@strawberry.django.type(ListingModel)
class Listing:
    listing_type: auto
    created_at: auto
    starts_at: auto
    ends_at: auto
    base_price_lovelace: auto
    is_active: auto
    base_price_ada: Optional[float]
