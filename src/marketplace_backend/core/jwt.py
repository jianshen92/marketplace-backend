from datetime import datetime
from typing import Any

from graphql import GraphQLType
from strawberry_django_jwt.object_types import TokenPayloadType
from strawberry_django_jwt.settings import jwt_settings

from marketplace_backend.accounts.models import User
from marketplace_backend.graphql_api.types import Info


def create_jwt_payload(user: User) -> TokenPayloadType:
    payload: dict[str, Any] = jwt_settings.JWT_PAYLOAD_HANDLER(user)
    return payload


def create_access_token(payload: TokenPayloadType) -> str:
    token: str = jwt_settings.JWT_ENCODE_HANDLER(payload)
    return token


def calculate_refresh_expires_in() -> int:
    utc_now = datetime.utcnow().timestamp()
    refresh_expires_in = (
        utc_now + jwt_settings.JWT_REFRESH_EXPIRATION_DELTA.total_seconds()
    )
    return int(refresh_expires_in)


# overrides buggy strawberry_django_jwt/middleware.py:allow_any implementation
def allow_any(info: Info, **kwargs: Any) -> bool:
    # will be fixed in newer version of strawberry
    field = info.parent_type.fields.get(info.field_name)  # type: ignore[attr-defined]

    if field is None:
        return False

    field_type = getattr(field.type, "of_type", None)

    return field_type is not None and any(
        [
            issubclass(class_type, GraphQLType) and isinstance(field_type, class_type)
            for class_type in tuple(jwt_settings.JWT_ALLOW_ANY_CLASSES)
        ]
    )
